using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Simple dialogue runner based on JSON file
/// <para>
/// This is basically a one-track runner; need to build something more complex
/// </para>
/// </summary>
namespace Dialogue {

    [System.Serializable]
    public class DialogueEntry {
        public string speaker;
        public string dialogue;
        public string meta;
        public string next;
    }

    /// <summary>
    /// Dialogue object for the entire scene
    /// Ordered content loaded directly from JSON
    /// </summary>
    [System.Serializable]
    public class SceneDialogue {

        /// <summary>
        /// Array of dialogue entries
        /// </summary>
        public DialogueEntry[] entries;

        /// <summary>
        /// The array index of the speaker entry
        /// </summary>
        int entryIndex = -1;

        public SceneDialogue(DialogueEntries entries) {
            this.entries = entries.entries;
        }

        /// <summary>
        /// Get the next DialogueEntry for a dialogue speaker
        /// </summary>
        /// <returns></returns>
        public DialogueEntry GetNextEntry() {
            entryIndex++;

            if (entryIndex < entries.Length) {
                return entries[entryIndex];
            }

            return null;
        }

        /// <summary>
        /// Load Scene dialogue for a specific level
        /// </summary>
        /// <param name="group">Name of file to open</param>
        /// <returns></returns>
        public static SceneDialogue LoadSceneDialogue(string group) {
            string levelName = SceneManager.GetActiveScene().name;
            DialogueEntries entries = LoadJsonFromFile<DialogueEntries>(level + "-" + group);

            return new SceneDialogue(entries);
        }

        /// <summary>
        /// Load JSON from file for a specific format
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="file"></param>
        /// <returns></returns>
        private static T LoadJsonFromFile<T>(string file) {
            // Read the json from the file into a string
            string rawJson = Resources.Load<TextAsset>(file).text;
            return JsonUtility.FromJson<T>(rawJson); ;
        }


        /// <summary>
        /// Wrapper for DialogueEntry elements when loaded from JSON
        /// </summary>
        [System.Serializable]
        public class DialogueEntries {
            public DialogueEntry[] entries;
        }
    }

}