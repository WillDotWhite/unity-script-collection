using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player {

    [RequireComponent(typeof(CharacterController))]
    public class Movement : MonoBehaviour {

        /// <summary>
        /// Movement speed
        /// </summary>
        [SerializeField]
        private float speed = 6.0f;

        /// <summary>
        /// CharacterController instance
        /// </summary>
        private CharacterController controller;

        void Start() {
            Cursor.lockState = CursorLockMode.Locked;
            controller = GetComponent<CharacterController>();
        }

        void Update() {
            // TODO: Rotate player to match camera facing?
            Move();
        }

        private void Move() {
            float verticalInput = Input.GetAxis("Vertical");
            float horizontalInput = Input.GetAxis("Horizontal");

            // 'Forwards' and 'Sideways' input is purely relative to the original camera facing;
            // this changes when the camera faces different directions
            Vector3 forwards = verticalInput * Camera.main.transform.forward;
            Vector3 sidewards = horizontalInput * Camera.main.transform.right;

            Vector3 moveDirection = new Vector3(forwards.x + sidewards.x, -9.81f, forwards.z + sidewards.z);

            // Reduce movement if moving at an angle; there's a better way to do this, but this'll do for now
            if (verticalInput != 0 && horizontalInput != 0) {
                moveDirection *= 0.7071f;
            }

            moveDirection = moveDirection * speed;

            // Move the controller
            controller.Move(moveDirection * Time.deltaTime);
        }
    }

}
