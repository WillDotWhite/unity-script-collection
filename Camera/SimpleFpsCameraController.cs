using UnityEngine;

public class SimpleFpsCameraController : MonoBehaviour {

    /// <summary>
    /// Left/right sensitivity
    /// </summary>
    [Range(1,20)]
    [Tooltip("Horizontal camera rotation sensitivity.")]
    public float horizontalSensitivity = 10.0f;

    /// <summary>
    /// Up/down sensitivity
    /// </summary>
    [Range(1,20)]
    [Tooltip("Vertical camera rotation sensitivity.")]
    public float verticalSensitivity = 10.0f;

    /// <summary>
    /// Current X-axis level rotation
    /// </summary>
    private float xRotation = 0f;

    /// <summary>
    /// Current Y-axis level rotation
    /// </summary>
    private float yRotation = 0f;

    /// <summary>
    /// Current horizontal input from mouse
    /// </summary>
    private float horizontalInput = 0f;

    /// <summary>
    /// Current vertical input from mouse
    /// </summary>
    private float verticalInput = 0f;


    private void Update() {
        horizontalInput = Mathf.Clamp(Input.GetAxis("Mouse X"), -1, 1);
        verticalInput = Mathf.Clamp(Input.GetAxis("Mouse Y"), -1, 1);
    }

    private void LateUpdate() {
        Look();
    }

    /// <summary>
    /// Update the Camera position
    /// </summary>
    private void Look() {
        // Translate input into angle change, and then into valid angle
        xRotation += horizontalInput * horizontalSensitivity;
        yRotation += verticalInput * verticalSensitivity;
        xRotation = ClampAngle(xRotation, -360f, 360f);
        yRotation = ClampAngle(yRotation, -65f, 65f);

        // Calculate the correct Look Quaternion for the current X/Y angles
        Quaternion xQuaternion = Quaternion.AngleAxis (xRotation, Vector3.up);
        Quaternion yQuaternion = Quaternion.AngleAxis (yRotation, -Vector3.right);
        Quaternion nextRotation = Quaternion.identity * xQuaternion * yQuaternion;

        // Update the camera's rotation
        Camera.main.transform.localRotation = Quaternion.Lerp(Camera.main.transform.localRotation, nextRotation, Time.deltaTime * 10.0f);
    }

    /// <summary>
    /// Shift a given angle into degrees format, and them clamp between given bounds
    /// </summary>
    /// <param name="angle"></param>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    private float ClampAngle(float angle, float min, float max) {
        if (angle < -360f) {
            angle += 360f;
        } else if (angle > 360f) {
            angle -= 360f;
        }

        return Mathf.Clamp(angle, min, max);
    }

}