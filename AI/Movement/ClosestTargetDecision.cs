using UnityEngine;

namespace AI {

    [CreateAssetMenu(menuName = "AI/Target/Closest")]
    public class ClosestTargetDecision : GetTargetDecision {

        protected override Entity GetTarget() {
            return GetClosestTarget();
        }


        /// <summary>
        /// Get the closest Entity target in the field
        /// </summary>
        /// <returns></returns>
        public Entity GetClosestTarget() {
            float closestDistance = float.MaxValue;
            Entity closestTarget = null;

            foreach (Entity target in Targets) {
                float distance = Vector3.Distance(target.transform.position, self.transform.position);

                if (distance < closestDistance) {
                    closestDistance = distance;
                    closestTarget = target;
                }
            }

            return closestTarget;
        }

    }

}
