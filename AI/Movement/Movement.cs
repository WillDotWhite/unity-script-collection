using AI;
using Combat.Specials;
using System.Collections;
using UnityEngine;

namespace AI {

    public class Movement : MonoBehaviour {

        /// <summary>
        /// Targeting system
        /// </summary>
        private TargetDecisionEngine targeting;

        /// <summary>
        /// Movement target
        /// </summary>
        public Entity Target {
            get {
                return targeting.Target;
            }
        }

        private void Awake() {
            targeting = GetComponent<TargetDecisionEngine>();
        }

        private void Update() {
            // No point attacking if there's nothing to attack
            if (!targeting.Target || !IsWithinRangeOfTarget()) {
                return;
            }

            // Within range of movement target
        }

        /// <summary>
        /// Am I within range to target?
        /// </summary>
        /// <returns></returns>
        public bool IsWithinRangeOfTarget() {
            return targeting.IsWithinRangeOfTarget(/* ... */);
        }

    }

}
