using System.Collections.Generic;
using UnityEngine;

namespace AI {

    public class TargetDecisionEngine : MonoBehaviour {

        /// <summary>
        /// Current Target
        /// </summary>
        public Entity Target {
            get {
                return TargetingDecision.Target;
            }
        }

        /// <summary>
        /// Decision driving Targeting
        /// </summary>
        public GetTargetDecision TargetingDecision;

        // Use this for initialization
        void Start() {
            TargetingDecision = Object.Instantiate(TargetingDecision);
            TargetingDecision.Initialise(gameObject);

            // Get targets and perform set up
        }

        /// <summary>
        /// Is self within range of target?
        /// </summary>
        /// <param name="range"></param>
        /// <returns></returns>
        public bool IsWithinRangeOfTarget(float range) {
            return TargetingDecision.IsWithinRangeOfTarget(range);
        }

        /// <summary>
        /// Get current available target list
        /// </summary>
        /// <returns></returns>
        public List<Entity> GetTargets() {
            return TargetingDecision.Targets;
        }
    }

}
