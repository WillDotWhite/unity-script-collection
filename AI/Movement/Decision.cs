using UnityEngine;

namespace AI {

    public abstract class Decision : ScriptableObject {

        /// <summary>
        /// Perform all setup for the decision engine
        /// </summary>
        /// <param name="gameObject"></param>
        public abstract void Initialise(GameObject gameObject);

        /// <summary>
        /// Make decision
        /// </summary>
        /// <returns></returns>
        protected abstract Entity GetTarget();

    }
}
