using System.Collections;
using UnityEngine;

namespace AI {

    /// <summary>
    /// Vision isn't an active FOV/LOS check, just an approximation
    /// </summary>
    public class Vision : MonoBehaviour {

        /// <summary>
        /// How far away can this Entity see?
        /// </summary>
        public float Range = 10f;

        /// <summary>
        /// Angle (+/-) of FOV
        /// </summary>
        [Range(0, 360)]
        public float Angle = 45f;

        /// <summary>
        /// The Target's transform
        /// </summary>
        [HideInInspector]
        public Transform Target;

        /// <summary>
        /// LayerMask for tracking target
        /// </summary>
        public LayerMask targetMask;

        /// <summary>
        /// LayerMask for obstacles between self and Target
        /// </summary>
        public LayerMask obstacleMask;

        /// <summary>
        /// Can I actively see the Target?
        /// </summary>
        public bool CanSeeTarget { get; private set; }

        private void Start() {
            Target = /* ... */;
            StartCoroutine("CheckForTarget", 0.2f);
        }

        /// <summary>
        /// Check for the Target's position
        /// </summary>
        /// <param name="delay"></param>
        /// <returns></returns>
        private IEnumerator CheckForTarget(float delay) {
            while (true) {
                yield return new WaitForSeconds(delay);
                CheckForTargetInSight();
            }
        }

        /// <summary>
        /// Check whether the Target is in my LOS
        /// </summary>
        private void CheckForTargetInSight() {
            // Is the Target within my view range?
            if (IsCloseEnoughToTarget()) {
                Debug.LogFormat("{0} is close enough to be able to see Target", name);

                // Is the Target within my FOV? 
                Vector3 dirToTarget = (Target.position - transform.position).normalized;
                if (IsTargetWithinFieldOfView(dirToTarget)) {
                    Debug.LogFormat("{0} is facing the right way to be able to see Target", name);

                    // Is my view of the Target unobstructed?
                    CanSeeTarget = IsTargetUnobstructed(dirToTarget);
                    if (CanSeeTarget) {
                        Debug.LogFormat("{0} can see Target!", name);
                    }
                }
            }
        } 

        /// <summary>
        /// Am I close enough to be able to see the Target?
        /// </summary>
        /// <returns></returns>
        private bool IsCloseEnoughToTarget() {
            // This should only ever have one entry, because the targetMask is specific to the Target
            Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, Range, targetMask);
            return targetsInViewRadius.Length > 0;

        }

        /// <summary>
        /// Is the Target within my field of vision?
        /// </summary>
        /// <returns></returns>
        private bool IsTargetWithinFieldOfView(Vector3 directionToTarget) {
            return Vector3.Angle(transform.forward, directionToTarget) < Angle / 2f;
        }

        /// <summary>
        /// Is there anything between me and the Target?
        /// </summary>
        /// <returns></returns>
        private bool IsTargetUnobstructed(Vector3 directionToTarget) {
            float distanceToTarget = Vector3.Distance(transform.position, Target.position);
            // Not sure why it's !Physics.Raycast() here, but it absolutely is.
            return !Physics.Raycast(transform.position, directionToTarget, distanceToTarget, obstacleMask);
        }

    }

}