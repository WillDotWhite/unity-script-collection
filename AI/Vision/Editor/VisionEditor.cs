using UnityEditor;
using UnityEngine;

namespace AI {

    [CustomEditor(typeof(Vision))]
    public class VisionEditor : Editor {

        private void OnSceneGUI() {
            // Draw Range circle
            Vision vision = (Vision) target;
            Handles.color = Color.white;
            Handles.DrawWireArc(vision.transform.position, Vector3.up, Vector3.forward, 360, vision.Range);

            // Draw FOV cone
            Vector3 fovMin = DirectionFromAngle(vision.transform, -vision.Angle / 2, false);
            Handles.DrawLine(vision.transform.position, vision.transform.position + ( fovMin * vision.Range ));
            Vector3 fovMax = DirectionFromAngle(vision.transform, vision.Angle / 2, false);
            Handles.DrawLine(vision.transform.position, vision.transform.position + ( fovMax * vision.Range ));

            // If the player is in sight, draw a line to them
            Handles.color = Color.red;
            if (vision.CanSeePlayer) {
                Handles.DrawLine(vision.transform.position, vision.Target.position);
            }
        }



        /// <summary>
        /// Get Vector3 direction from a given angle
        /// </summary>
        /// <param name="angleInDegrees"></param>
        /// <returns></returns>
        private Vector3 DirectionFromAngle(Transform transform, float angleInDegrees, bool angleIsGlobal) {
            if (!angleIsGlobal) {
                angleInDegrees += transform.eulerAngles.y;
            }

            return new Vector3(
                Mathf.Sin(angleInDegrees * Mathf.Deg2Rad),
                0,
                Mathf.Cos(angleInDegrees * Mathf.Deg2Rad)
            );
        }


    }

}