# Unity Script Collection

A collection of built/discovered Unity scripts that would be handy to have to hand.

It's mostly code that either comes up a lot (Player vision/movement), or more complex code that will save time to template out (like more error-resistant AI movement behaviours).

## TODO
Review, test and [comment and include|rebuild and include|more to permenant "external resource" list]:

- [UnityPatterns/ObjectPool](https://github.com/UnityPatterns/ObjectPool/blob/master/Assets/ObjectPool/Scripts/ObjectPool.cs)
- [UnityPatterns/AutoMotion](https://github.com/UnityPatterns/AutoMotion/blob/master/Assets/AutoMotion/Scripts/Auto.cs)
- [daemon3000/InputManager](https://github.com/daemon3000/InputManager)
- [cjddmut/Unity-2D-Platformer-Controller](https://github.com/cjddmut/Unity-2D-Platformer-Controller)
- [prime31/CharacterController2D](https://github.com/prime31/CharacterController2D)
- [phosphoer/SuperMarioGalaxyGravity](https://gist.github.com/phosphoer/a283cdbeca5d2160d5eed318d0362826)
- [ellioman/ShaderProject](https://github.com/ellioman/ShaderProject)
- [keijiro/SkyboxPlus](https://github.com/keijiro/SkyboxPlus)
- [tragget/UnitySpriteShaders](https://github.com/traggett/UnitySpriteShaders)
- [BrokenVector/LowPolyShaders](https://github.com/BrokenVector/LowPolyShaders)
- [dotnet-state-machine/stateless](https://github.com/dotnet-state-machine/stateless)

### RESEARCH

- [Kvant scripts](https://github.com/search?q=kvant+user%3Akeijiro&type=Repositories)
- [Unity Script Collection](https://github.com/michidk/Unity-Script-Collection)
- [Kink3d/SimpleTraffic](https://github.com/Kink3d/SimpleTraffic)
- [keijiro](https://github.com/keijiro)