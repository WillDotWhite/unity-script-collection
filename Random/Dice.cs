using System;

namespace Random {

    public class Dice {

		/// <summary>
		/// Roll damage for a standardised dice format string
		/// </summary>
		/// <param name="rollString">Formatted dice roll string</param>
		public static int Roll(string rollString) {
			int total = 0;
			string[] rolls = rollString.Split('+');

            // Roll every dice in rollString
			foreach(string roll in rolls) {
				if (roll.IndexOf('d') > -1) {
					// Split XdY into [X, Y]
					string[] components = roll.Split('d');
					int quantity = Int32.Parse(components[0]);
					int value = Int32.Parse(components[1]);

                    for(int i = 0; i < quantity; i++) {
                        // Random range is [min inc, max excl], so add +1 for int
                        total += (int) UnityEngine.Random.Range(1, value + 1);
                    }
				} else {
					// Value was a raw +X, not +XdY
					total += Int32.Parse(roll);
				}
			}

            return total;
		}

	}

}